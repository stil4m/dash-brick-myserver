function myserver_class (name) {

    var thisObject = this;
    this.name = name;
    var elementID = this.name;
    var path = '/plugins/' + elementID;
    var settingsElementId = 'myServerSettings'

    this.render = function() {
        dash.render(path + '/', elementID);
    };

    this.settings = function() {
        dash.modalSettings(
            path + '/settings',
            elementID,
            settingsElementId,
            this.render
        );
    };

    this.addServerSetting = function() {
        var aliasInput = $("#"+settingsElementId).find("form.addServerForm").find("#alias");
        var hostInput = $("#"+settingsElementId).find("form.addServerForm").find("#host");
        var portInput = $("#"+settingsElementId).find("form.addServerForm").find("#port");
        $.post(
            path + '/add',
            {
                'alias': aliasInput.val(),
                'host': hostInput.val(),
                'port': portInput.val()
            },
            function(response) {
                thisObject.addNewServerToTable(response)
            },
            'json'
        );
    };

    this.deleteServerSetting = function(object) {
        var row = $(object).closest('tr');
        var serverName = row.find('td.aliasData').text();
        $.post(
            path + '/delete',
            {
                'alias': serverName
            }
        );
        var index = row.prevAll().length;
        row.parent().get(0).deleteRow(index);
    };

    this.exitSettings = function() {
        dash.hideModalSettings(settingsElementId)
    };

    this.addNewServerToTable = function(response) {
        var errorslistElement = $("#"+settingsElementId).find("ul.addServerFormErrorsList");
        errorslistElement.empty();
        if (response.success) {
            var aliasInput = $("#"+settingsElementId).find("form.addServerForm").find("#alias");
            var hostInput = $("#"+settingsElementId).find("form.addServerForm").find("#host");
            var portInput = $("#"+settingsElementId).find("form.addServerForm").find("#port");
            var serversList = $("#"+settingsElementId).find("table.serverSettingsList");

            serversList.append('<tr><td class="aliasData">' + aliasInput.val() + '</td><td>'+ hostInput.val() + '</td><td>'+ portInput.val() + '</td><td><a onclick="' + thisObject.name + '.deleteServerSetting(this)" class="btn btn-danger">Delete</a></td></tr>');
            aliasInput.val('');
            hostInput.val('');
            portInput.val('');
        } else {
            for (var error in response.errors) {
                errorslistElement.append('<li>' + response.errors[error] + '</li>');
            }
        }
    };
};
