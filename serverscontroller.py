import cherrypy
from .lib.server import Server
from jinja2 import Environment, FileSystemLoader
from app.controllers import BaseController

class ServersController(BaseController):

    def __init__(self, servers_store, prefix):
        BaseController.__init__(self)
        self.plugin_name = prefix.split('/')[-1]
        self.env = Environment(loader=FileSystemLoader(prefix.lstrip('/') + '/templates'))
        self.servers_store = servers_store

    @cherrypy.expose
    def index(self):
        template = self.env.get_template('index.html')
        servers = self.servers_store.all_servers()
        return template.render(servers=servers, name=self.plugin_name)

    @cherrypy.expose
    def settings(self):
        template = self.env.get_template('settings.html')
        servers = self.servers_store.all_servers()
        return template.render(servers=servers, name=self.plugin_name)

    @cherrypy.expose
    def add(self, port='', host='', alias=''):
        if port.isdigit() == False:
            server = Server(host=str(host), port=0, name=str(alias))
            errors = self.servers_store.validate_server(server)
            errors.append('Server must have a valid port number.')
            return self.post_failure_with_errors(errors)

        server = Server(host=str(host), port=int(port), name=str(alias))
        errors = self.servers_store.validate_server(server)

        if errors:
            return self.post_failure_with_errors(errors)
        self.servers_store.append(server)
        return self.post_success_with_data(server.to_json())

    @cherrypy.expose
    def delete(self, alias=''):
        self.servers_store.remove_by_name(str(alias))
