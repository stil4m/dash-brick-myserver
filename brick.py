from lib.dpmsupdater import Updater
from lib.serveraddressstore import ServerAddressStore
from .serverscontroller import ServersController

class Brick():

    address_store = None
    updater = None
    
    def __init__(self, mapper, prefix, path):
        self.mapper = mapper
        self.prefix = prefix
        self.name = self.prefix.split('/')[-1]
        self.path = path
        self.address_store = ServerAddressStore(path + '/db')
        self.setup_mapper()
    
    def setup_mapper(self):
        if self.prefix and self.mapper:
            servers_controller = ServersController(self.address_store, prefix=self.prefix)
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + ''), controller=servers_controller, action = 'index')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/'), controller=servers_controller, action = 'index')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/settings'), controller=servers_controller, action = 'settings')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/settings/'), controller=servers_controller, action = 'settings')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/add'), controller=servers_controller, action = 'add', conditions=dict(method=['POST']))
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/delete'), controller=servers_controller, action = 'delete', conditions=dict(method=['POST']))

    def update(self):
        if not self.updater:
            configFile = self.path + '/config.ini'
            self.updater = Updater(configFile, self.address_store)
        self.updater.execute()

    def get_javascript(self):
        js = "<script type=\"text/javascript\" src=\""+ self.prefix + "/brick.js\"></script>\n"
        js += "<script>var " + self.name + " = new myserver_class('"+self.name+"')</script>\n"
        js += "<script>"+ self.name +".render()</script>"
        return js

    def get_real_name(self):
        return 'dash-brick-myserver'

    def timeout(self):
        return 20