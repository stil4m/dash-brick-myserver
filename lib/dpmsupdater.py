__author__ = 'mstijlaart'

import socket

class Updater:
    
    def __init__(self, config_file_path, servers_store):
        self.servers_store = servers_store

    def execute(self):
        servers = self.servers_store.all_servers()
        for server in servers:
            self.ping_server(server)

    def ping_server(self, server):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((server.host, server.port))
            s.close()
            server.set_status_available()
        except socket.error, msg:
            s.close
            server.set_status_unavailable()

        self.servers_store.update_server(server)
