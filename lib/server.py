class Server:
    
    status = "UNKOWN"
    
    def __init__(self, name, host, port):
        self.name = name
        self.host = host
        self.port = port
    
    @property
    def get_port(self):
        return self.port
                
    @property
    def get_host(self):
        return self.host
        
    @property
    def get_name(self):
        return self.name
        
    @property
    def get_status(self):
        return self.status
        
    def set_status_available(self):
        self.status = "AVAILABLE"
        
    def set_status_unavailable(self):
        self.status = "UNAVAILABLE"

    def to_json(self):
        return "{ \"port\":" + str(self.port) + ", \"host\":\""+self.host + "\", \"name\":\"" +self.name+ "\"}"