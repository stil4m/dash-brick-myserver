__author__ = 'mstijlaart'

import shelve

class ServerAddressStore:

    def __init__(self, store_path):
        store_path += '/Store.shelf'
        self.shelve = shelve.open(filename=store_path)


    def append(self, new_address):
        self.shelve[new_address.name] = new_address
        self.save()

    def __order_addresses_by_date(self, addresses):
        addresses.sort(key=lambda address: address.name, reverse=False)

    def all_servers(self):
        addresses = []
        for k in self.shelve.keys():
            addresses.append(self.shelve[k]);
        return addresses

    def remove_by_name(self, name):
        del self.shelve[name]
        self.save()

    def update_server(self, server):
        if self.shelve[server.name] != None:
            self.shelve[server.name] = server

    def save(self):
        self.shelve.sync()

    def validate_server(self, server):
        errors = []
        if not server.name:
            errors.append('Server must have a alias.')
        elif server.name in self.shelve:
            errors.append('Server alias is already taken.')
        if not server.host:
            errors.append('Server must have a host name.')
        return errors